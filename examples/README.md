# Program Examples

These are misc examples of using ZL assembly.

Use the `run_file.sh` script at the root of the repository to compile and
execute an example.

`./run_file.sh ./examples/random_numbers.zla` `./run_file.sh
./examples/game/main.zla`

## Game

The game example demonstrates a lot of functionality including the text grid
display, arrow key keyboard input, random number generation, and subroutines.
Use the arrow keys to move the player and collect the treasure.
