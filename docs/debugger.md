# Emulator Debugger

When the emulator executes the `DEBUG` instruction, the emulation debugger is
started and execution is paused. The debugger consists of a text prompt where
commands can be entered to print memory, internal CPU state, step through
instructions or continue execution.

## Debugger Commands

### continue

**Aliases:** cont

Continue normal execution.

### disable

**Aliases:** dis

Disable the debugger for the rest of program execution. Any future `DEBUG`
instructions will become noops.

### enable

**Aliases:** en

Reenable the debugger after a `disable`. This is only effective after a `disable`
and before `continue`. Otherwise, the debugger is never started again to enable
it.

### exit

**Aliases:** quit

Stop program execution and exit emulator.

### memory ADDRESS

**Aliases:** mem

Print a 16-bit memory location. ADDRESS is interpreted as hexadecimal. The
address may begin with "0x", "x",  or nothing. For example, all the following
are the same: "0x1234", "x1234", "1234".

### memoryblock ADDRESS

**Aliases:** memb

Print a 256 byte block of memory starting at ADDRESS. ADDRESS is interpreted as
hexadecimal. The address may begin with "0x", "x",  or nothing. For example, all
the following are the same: "0x1234", "x1234", "1234".

### next

Print the next instruction.

### registers

**Aliases:** reg

Print internal CPU state including PC, SP, ISP, and status flags.

### stack

Print the instruction stack.

### step

Execute the next instruction and reenter the debugger.
