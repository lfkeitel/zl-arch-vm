# ZL Architecture

The ZL architecture is a 16-bit general purpose data processing system. The
instruction set utilizes a stack based processing model instead of the traditional
register architecture.

## Memory

This emulator provides a full 16-bit, byte-addressable memory space. Memory is
broken up into several segments for code, read-only data, and I/O devices.
The memory layout is available [here](memory_layout.md).

## Stacks

All data processing works on an instruction stack, not to be confused with
the program stack. The instruction stack resides in the CPU and replaces
registers from other architectures. Data processing is done by manipulating
this stack. Only the top item is immediately available for work. Many instructions
take multiple items from the stack for processing. The traditional program or
software stack is made available through the PUSH and POP instructions. The
program stack resides in main memory and can be accessed by normal memory
load/store instructions.
