# ZL Architecture Documentation

- [Architecture](architecture.md)
- [Instruction Set](assembly.md)
- [Memory Layout](memory_layout.md)
- [I/O Devices](io_devices.md)
- [Machine Language](instructions.md)
- [Debugger](debugger.md)
