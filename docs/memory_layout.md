# Memory Layout

```
0x0000 - 0x01FF :: Interrupt Handlers - 512 bytes (256 interrupts)
0x0200 - 0x0FFF :: I/O - 3584 bytes
0x1000 - 0x1FFF :: Read-only data - 4096 bytes
0x2000 - 0xFFFF :: Code data - 57344 bytes
```

## Interrupt Handlers

```
0x0000 :: Reset Interrupt - Address of starting code
0x0002 :: Software interrupt
0x0004 - 0x01FF :: Reserved
```
