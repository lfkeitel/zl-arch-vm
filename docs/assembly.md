# ZL Assembly Instruction Set

ZL assembly is a simplified instruction set to work with the stack based ZL
architecture. Most instructions don't have arguments.

## Abbreviations

- TOIS - Top of instruction (CPU) stack
- TOPS - Top of program (software) stack

## Data Size

Memory is byte addressable. All instructions operate on 16-bit values. `LOAD`
will load the contents of `[address:address+1]`, likewise `STR` stores to
`[address:address+1]`.

## Syntax

### Code

All instructions are written using case-insensitive mnemonics. For instructions
that require arguments, they follow immediately after the mnemonic.

```
    NOOP
    LOAD #16
    DROP
    HALT
```

Instructions are typically indented at least one level from the left to
differentiate them from labels.

### Labels

Labels are used to mark a specific point in code. Labels can be used with `LOAD`
or `STR` instructions for memory manipulation. They can also be used with `FDB`
when an addresses is needed in constant memory for example with interrupt
vectors. Labels begin in the left most column on separate line. Labels begin
with a colon ':' when they're declared.

Labels can be used anywhere a 16-bit value is valid.

```
    ORG RESET_VECTOR
    FDB main   ; Store the address of "main" in memory

    ORG 0x2000
:main      ; Mark the location 0x2000 as "main"
    HALT
```

### Immediate Values

One variant of the `LOAD` instruction takes an immediate value to load into the
CPU. Immediate values are indicated with a leading pound sign '#'.

```
    LOAD #0x1234    ; Load the literal value 0x1234
    LOAD 0x1234     ; Load the contents at address 0x1234
```

## Literals

ZL assembly has several literal:

### Ints

Integers are a primitive data type and can be expressed in base 2, 16, and 10:

- Hex 8-bit: `0x12`
- Hex 16-bit: `0x1234`
- Decimal: `54`
- Binary: `!01100101`

### Strings

Strings can be used with `FCB` or `FDB` to create constant memory:

`FDB "Hello, World"`

## Assembly Meta Instructions

These instructions only affect the compile step. They are not real CPU
instructions.

- `ORG 0x1000` - Set address of following code.
- `FCB 0x12,0x34,0x56,0x78` - Form constant byte, define a comma separated list
  of 8-bit values.
- `FDB 0x1234,0x5678` - Form double byte, define a comma separated list of
  16-bit values, strings used with FDB will have each character defined as a
  16-bit value.
- `RMB 16` - Reserve N bytes of memory.
- `DEFINE IDENT 0x1234` - Define a constant 16-bit value to a label.
- `DEBUG` - Start a debug session, this is an actual CPU instruction unlike the
  rest. See the [debugger docs](debugger.md) for more information.
- `INCLUDE ""` - Include another assembly file into the current file. File paths
  are relative to the including file.

## Loading and Storing Data

Instructions consume their arguments from the instruction stack.

- `LOAD` - Load data onto the instruction stack from memory.
    - `LOAD #0x1234` - Loads the literal value 0x1234.
    - `LOAD 0x1234` - Loads the contents of memory address 0x1234.
    - `LOAD` - Takes the address from TOIS and loads the contents at that
      address.
- `STR` - Store data from the instruction stack to memory.
    - `STR 0x1234` - Store TOIS to address 0x1234.
    - `STR` - Stores TOIS into the address also stored on the stack below the
      data.
- `LOADSP` - Take TOIS and store it to the program stack pointer.
- `PUSHSP` - Load the program stack pointer onto the instruction stack.

```
    LOAD #0x1234
    LOAD #0x5234
    STR ; Store 0x5234 to memory address 0x1234

    LOAD #0x5234
    STR 0x1234 ; Store 0x5234 to memory address 0x1234
```

## Misc Instructions

- `NOOP` - Do nothing for one instruction cycle.
- `HALT` - Stop all CPU execution (exits the emulator).

## Arithmetic Operations

Instructions consume their arguments from the instruction stack.

- `ADD` - Add the two TOIS values and push result.
- `SUB` - Subtract the two TOIS values and push result.
- `MUL` - Multiply the two TOIS values and push result.
- `DIV` - Divide the two TOIS values and push result.
- `MOD` - Divide the two TOIS values and push the remainder.

## Logic Operations

Instructions consume their arguments from the instruction stack.

- `OR` - Logically "or" the two TOIS values and push result.
- `AND` - Logically "and" the two TOIS values and push result.
- `XOR` - Logically "xor" the two TOIS values and push result.
- `NOT` - Negate TOIS.

- `ASHR` - Shift TOIS+1 right by TOIS bit positions preserving the sign bit.
  Equivalent to division by 2^n.
- `ASHL` - Shift TOIS+1 left by TOIS bit positions preserving the sign bit if
  possible. Equivalent to multiplication by 2^n.
- `LSHR` - Shift TOIS+1 right by TOIS bit positions.
- `LSHL` - Shift TOIS+1 left by TOIS bit positions.

## Vectored Operations

These instructions operate on multiple stack items at the same time.

- `ADDMV` - TOIS is the size of vector as N, TOIS+1 is the operand. Adds the
    operand to top N stack items after poping arguments.
- `MULMV` - TOIS is the size of vector as N, TOIS+1 is the operand. Multiplies
    the operand to top N stack items after poping arguments.
- `ANDMV` - TOIS is the size of vector as N, TOIS+1 is the operand. Logically
    ands the operand to top N stack items after poping arguments.
- `ORMV` - TOIS is the size of vector as N, TOIS+1 is the operand. Logically ors
    the operand to top N stack items after poping arguments.

## Instruction Stack Manipulation

- `DUP` - Duplicate the TOIS value and push it.
- `DROP` - Destroy the TOIS value.
- `REV` - Reverse the two TOIS values so TOIS+1 becomes the new TOIS.
- `SRUP` - Shift N items from the TOIS upward X slots.
- `SRDN` - Shift N items from the TOIS downward X slots. N and X are defined by
    the TOS item where the first byte is the number of items to work on and the
    second byte is the number of rotations to perform.

## Program Stack Manipulation

Instructions consume their arguments from the instruction stack.

- `PUSH` - Remove the TOIS value and push it to the program stack. The SP is
  decremented by 2.
- `POP` - Load the TOPS value onto the instruction stack. The SP is incremented
  by 2.

## Comparison

- `TEST` - Compare the two TOIS values without consuming them.

## Branching

Instructions consume their arguments from the instruction stack.

- `JMP` - Always jump to the address at TOIS.
- `JMPZ` - Jump to TOIS only if the Z flag is set.
- `JMPG` - Jump to TOIS only if Z flag is clear and N flag is set
- `JMPL` - Jump to TOIS only if Z flag is clear and N flag is clear
- `JMPNZ` - Jump to TOIS only if the Z flag is clear.

## Subroutines

Instructions consume their arguments from the instruction stack.

- `CALL` - Jump to TOIS. The current PC is pushed to the program stack before
  jumping.
- `RET` - Pop the PC from the program stack and jump to it.

## Interrupts

- `SWI` - Trigger a software interrupt. The current PC and FLAGS registers are
  pushed to the program stack. The PC is then set to the address in the software
  interrupt slot of the interrupt vector table.
- `IRET` - Return from an interrupt handler. Pop the FLAGS and PC registers from
  the program stack. Resume normal execution.
