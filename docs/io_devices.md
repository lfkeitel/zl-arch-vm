# I/O Devices

## Text/Video Display

### Memory Layout

- Data Memory: 0x0200 - 0x029F (in color mode, only 0x0200-0x0201)
- Command Port: 0x02A0

### Commands

- DISPLAY = 0x10 - Display the current line, overwrites current line
- DISPLAY_GRID_COLOR = 0x11 - Display the 80x25 character grid whose address is stored
  in the first word of the line buffer (0x0200) with color.
- NEWLINE = 0x15 - Start a new line, line buffer is reset to zeros
- CLEAR   = 0x16 - Clear line buffer

### Description

The data memory represents a single line of output. Each character is a single
16-bit word. The line is not displayed until the DISPLAY command has been given
to the printer via its command port. The printer has no colors and only supports
ASCII output. The memory is reset to 0x00 after the line is displayed.

When using the color grid display mode, only the first word of data memory is
used. It must contain the memory address for the full 80x25 character grid. Like
text mode, each character is 16-bits long however the higher bit is used to
store color information. Each color consists of 3 bits and a "bright" bit
allowing for 16 colors.

```
Bit 76543210
    ||||||||
    |||||^^^-foreground color
    ||||^----foreground color bright bit
    |^^^-----background color
    ^--------background color bright bit
```

The colors are:

| Number | Name       | Number + bright bit | Name          |
|--------|------------|---------------------|---------------|
| 0      | Black      | 0+8=8               | Dark Gray     |
| 1      | Blue       | 1+8=9               | Light Blue    |
| 2      | Green      | 2+8=A               | Light Green   |
| 3      | Cyan       | 3+8=B               | Light Cyan    |
| 4      | Red        | 4+8=C               | Light Red     |
| 5      | Magenta    | 5+8=D               | Light Magenta |
| 6      | Brown      | 6+8=E               | Yellow        |
| 7      | Light Gray | 7+8=F               | White         |

To get white text on a black background, the higher byte should be set to
`0x0F`. A byte of all zeros will print black text on a black background making
it unreadable.

At the moment, **color is not supported** but the 80x25 grid mode is. For an example
if this mode, see the game example program.

### Examples

Text-mode:

```
    ORG 0x0000
    FDB main

    ORG 0x2000
:main
    LOAD #0x02A0        ; Command port for line display
    DUP
    LOAD #0x10          ; Display line buffer
    STR
    LOAD #0x15          ; Go to new line
    STR
    HALT

    ORG 0x0200          ; Populate line buffer, cheating
    FDB "Hello, World"
```

80x25 Grid:

```
    ORG 0x0000
    FDB main

    ORG 0x2000
:main
    LOAD #grid_buffer
    STR 0x0200          ; Store the buffer address in the line buffer
    LOAD #0x11          ; Display text grid
    STR 0x02A0
    HALT

    ORG 0xF000          ; Start of grid buffer
:grid_buffer
    ; "Hello!"
    FDB 0x0F48,0x0F65,0x0F6C,0x0F6C,0x0F6F,0x0F21
    RMB 3994 ; 4000 - (len("Hello!") * 2)
```

## Keyboard Input

### Memory Layout

- Command Port: 0x02A2
- Data Port: 0x02A4

### Commands

- GET_KEY = 0x10 - Get a single key input from keyboard

### Description

Keyboard input is a text-based peripheral for interactive applications.
Character codes are retrieved by sending a GET_KEY command to the peripheral and
then reading from the data port. If an invalid key was entered, the data port
will be 0 otherwise it will be the ASCII value of the entered key.

Non alphanumeric keycodes:

| Key Name  | Keycode  |
|-----------|----------|
| Ctrl D    | 0x04     |
| Backspace | 0x08     |
| Ctrl Z    | 0x1A     |
| Escape    | 0x1B     |
| Delete    | 0x7F     |
| PageDown  | 0xEE     |
| PageUp    | 0xEF     |
| F1        | 0xF0     |
| F2        | 0xF1     |
| F3        | 0xF2     |
| F4        | 0xF3     |
| F5        | 0xF4     |
| F6        | 0xF5     |
| F7        | 0xF6     |
| F8        | 0xF7     |
| F9        | 0xF8     |
| F10       | 0xF9     |
| F11       | 0xFA     |
| F12       | 0xFB     |
| Down      | 0xFC     |
| Up        | 0xFD     |
| Right     | 0xFE     |
| Left      | 0xFF     |

## Clock

### Memory Layout

- Command Port: 0x02A6
- Data Port: 0x02A8-0x02AE

### Commands

- GET_TIME = 0x10 - Write the current time as a 64-bit integer to data memory.

### Description

Return the current time in seconds since January 1, 1970 12:00:00AM. The
timestamp is 64-bits or 4 words long.
