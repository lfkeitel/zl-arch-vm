# Proposal - SIMD Instruction Set

**Status**: In progress

This proposal outlines the creation of a single instruction multiple data (SIMD)
instruction set.

## Instructions

- `{OP}M` - Perform the operation using several stack items as multiple
  operands.
- `{OP}MV` - Perform the operation on several stack items as if they were
  individual operations.

`{OP}` being:

- ADDM
- ADDMV (Implemented)
- SUBM
- SUBMV
- MULM
- MULMV (Implemented)
- DIVM
- DIVMV
- MODMV
- ORM
- ORMV (Implemented)
- ANDM
- ANDMV (Implemented)
- XORM
- XORMV
- NOTMV
- ASHRMV
- ASHLMV
- LSHRMV
- LSHLMV
- DROPM
- PUSHM
- POPM

Would require 23 new opcodes.

## Description

Both variants work on multiple stack items instead of just two.

The `M` variant performs an operation successively down the instruction stack
for N number of items. N is pushed to TOS.

The `MV` variant performs an operation on multiple items in the stack
simultaneously. The number of items N and the operand for binary operations are
pushed to the instruction stack and the instruction is executed.

## Exceptions

Many instructions don't have an `M` variant. In these cases, it doesn't make
sense to use multiple stack items as operands to the instruction. These
instruction only make sense in vector contexts.

Likewise, many instruction don't have `MV` variants. It doesn't make sense for
these instructions to work on a vector of values but instead on multiple
operands. These instructions only have an `M` variant instruction.

## Examples

### Example 1 - ADDM

Starting stack:

```
 |        |
 | 0x1234 |
 | 0x1234 |
 | 0x1234 |
 | 0x5F4D |
 +--------+
```

Instructions:

```
LOAD #3     ; Number of stack items to add together
ADDM
```

Result:

```
 |        |
 | 0x369C |
 | 0x5F4D |
 +--------+
```

What Happened:

ADDM adds the top 3 stack items together.

### Example 2 - ADDMV

Starting stack:

```
 |        |
 | 0x1234 |
 | 0x1234 |
 | 0x1234 |
 | 0x5F4D |
 +--------+
```

Instructions:

```
LOAD #0x0010    ; Value to add to stack items
LOAD #3         ; Number of stack items to perform addition
ADDMV
```

Result:

```
 |        |
 | 0x1244 |
 | 0x1244 |
 | 0x1244 |
 | 0x5F4D |
 +--------+
```

What Happened:

ADDMV adds the value 0x0010 to each of the top 3 stack items.
