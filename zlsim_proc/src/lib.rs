extern crate proc_macro;
extern crate quote;
extern crate syn;

use proc_macro::TokenStream;
use proc_macro2::{self, Literal, Punct, Spacing, Span};
use quote::quote;
use quote::TokenStreamExt;
use syn::punctuated::Pair;
use syn::{parse_macro_input, Data, DataEnum, DeriveInput, Expr, Ident, Lit, LitInt};

macro_rules! punct_pair {
    ($name:ident) => {
        match $name {
            Pair::Punctuated(v, _) => v,
            Pair::End(v) => v,
        }
    };
}

struct OpCodesEnum {
    ident: Ident,
    codes: Vec<OpCodeEnum>,
}

impl OpCodesEnum {
    fn from_derive(ident: Ident, input: &DataEnum) -> OpCodesEnum {
        let mut codes = Vec::new();
        let mut has_default = false;

        for item in input.variants.pairs() {
            let variant = punct_pair!(item);

            let name = &variant.ident;
            let discriminant = match &variant.discriminant {
                Some((_, d)) => d,
                None => panic!("Opcodes require an explicit value"),
            };

            let default = if !(&variant.attrs).is_empty() {
                let first_segment = &variant.attrs[0].path.segments.first().unwrap();
                punct_pair!(first_segment).ident == "default"
            } else {
                false
            };

            if has_default && default {
                panic!("Only one default variant allowed");
            }

            if let Expr::Lit(expr) = discriminant {
                if let Lit::Int(value) = &expr.lit {
                    codes.push(OpCodeEnum {
                        name: name.clone(),
                        value: value.clone(),
                        default,
                    });

                    has_default = has_default || default;
                }
            }
        }

        if !has_default {
            panic!("No default u8 opcode mapping defined");
        }

        OpCodesEnum { ident, codes }
    }

    fn to_from_u8_list(&self, tokens: &mut proc_macro2::TokenStream) {
        let mut default = None;

        for code in &self.codes {
            tokens.append(Literal::u8_suffixed(code.value.value() as u8));
            tokens.append(Punct::new('=', Spacing::Joint));
            tokens.append(Punct::new('>', Spacing::Alone));
            tokens.append(self.ident.clone());
            tokens.append(Punct::new(':', Spacing::Joint));
            tokens.append(Punct::new(':', Spacing::Joint));
            tokens.append(code.name.clone());
            tokens.append(Punct::new(',', Spacing::Alone));

            if code.default {
                default = Some(code);
            }
        }

        if let Some(default) = default {
            tokens.append(Ident::new("_", Span::call_site()));
            tokens.append(Punct::new('=', Spacing::Joint));
            tokens.append(Punct::new('>', Spacing::Alone));
            tokens.append(self.ident.clone());
            tokens.append(Punct::new(':', Spacing::Joint));
            tokens.append(Punct::new(':', Spacing::Joint));
            tokens.append(default.name.clone());
            tokens.append(Punct::new(',', Spacing::Alone));
        }
    }

    fn to_display_list(&self, tokens: &mut proc_macro2::TokenStream) {
        for code in &self.codes {
            tokens.append(self.ident.clone());
            tokens.append(Punct::new(':', Spacing::Joint));
            tokens.append(Punct::new(':', Spacing::Joint));
            tokens.append(code.name.clone());
            tokens.append(Punct::new('=', Spacing::Joint));
            tokens.append(Punct::new('>', Spacing::Alone));
            tokens.append(Literal::string(&code.name.to_string()));
            tokens.append(Punct::new(',', Spacing::Alone));
        }
    }
}

#[derive(Clone)]
struct OpCodeEnum {
    name: Ident,
    value: LitInt,
    default: bool,
}

#[proc_macro_derive(OpcodeExtras, attributes(default))]
pub fn opcode_extras(tokens: TokenStream) -> TokenStream {
    let input = parse_macro_input!(tokens as DeriveInput);

    let data_enum = match input.data {
        Data::Enum(d) => d,
        _ => panic!("opcode extras can only be used on enums"),
    };

    let name = input.ident;

    let codes = OpCodesEnum::from_derive(name.clone(), &data_enum);

    let mut from_u8_list = proc_macro2::TokenStream::new();
    codes.to_from_u8_list(&mut from_u8_list);

    let mut display_list = proc_macro2::TokenStream::new();
    codes.to_display_list(&mut display_list);

    proc_macro::TokenStream::from(quote! {
        impl From<u8> for #name {
            fn from(i: u8) -> #name {
                match i {
                    #from_u8_list
                }
            }
        }

        impl ::std::fmt::Display for #name {
            fn fmt(&self, f: &mut ::std::fmt::Formatter) -> ::std::fmt::Result {
                write!(
                    f,
                    "{}",
                    match self {
                        #display_list
                    }
                )
            }
        }

        impl ::std::fmt::Debug for #name {
            fn fmt(&self, f: &mut ::std::fmt::Formatter) -> ::std::fmt::Result {
                write!(f, "{}", self)
            }
        }
    })
}

// Lexer Token Derive
struct TokensEnum {
    ident: Ident,
    codes: Vec<TokenDiscrim>,
}

impl TokensEnum {
    fn from_derive(ident: Ident, input: &DataEnum) -> TokensEnum {
        let mut codes = Vec::new();

        for item in input.variants.pairs() {
            let variant = punct_pair!(item);

            let name = &variant.ident;

            let keyword = if !(&variant.attrs).is_empty() {
                let first_segment = &variant.attrs[0].path.segments.first().unwrap();
                punct_pair!(first_segment).ident != "not_keyword"
            } else {
                true
            };

            codes.push(TokenDiscrim {
                name: name.clone(),
                keyword,
            });
        }

        TokensEnum { ident, codes }
    }

    fn to_lookup_ident_list(&self, tokens: &mut proc_macro2::TokenStream) {
        for code in &self.codes {
            if !code.keyword {
                continue;
            }

            tokens.append(Literal::string(&code.name.to_string()));
            tokens.append(Punct::new('=', Spacing::Joint));
            tokens.append(Punct::new('>', Spacing::Alone));
            tokens.append(self.ident.clone());
            tokens.append(Punct::new(':', Spacing::Joint));
            tokens.append(Punct::new(':', Spacing::Joint));
            tokens.append(code.name.clone());
            tokens.append(Punct::new(',', Spacing::Alone));
        }
    }

    fn to_display_list(&self, tokens: &mut proc_macro2::TokenStream) {
        for code in &self.codes {
            tokens.append(self.ident.clone());
            tokens.append(Punct::new(':', Spacing::Joint));
            tokens.append(Punct::new(':', Spacing::Joint));
            tokens.append(code.name.clone());
            tokens.append(Punct::new('=', Spacing::Joint));
            tokens.append(Punct::new('>', Spacing::Alone));
            tokens.append(Literal::string(&code.name.to_string()));
            tokens.append(Punct::new(',', Spacing::Alone));
        }
    }
}

#[derive(Clone)]
struct TokenDiscrim {
    name: Ident,
    keyword: bool,
}

#[proc_macro_derive(TokenExtras, attributes(not_keyword))]
pub fn token_extras(tokens: TokenStream) -> TokenStream {
    let input = parse_macro_input!(tokens as DeriveInput);

    let data_enum = match input.data {
        Data::Enum(d) => d,
        _ => panic!("TokenExtras can only be used on enums"),
    };

    let name = input.ident;

    let codes = TokensEnum::from_derive(name.clone(), &data_enum);

    let mut lookup_ident_list = proc_macro2::TokenStream::new();
    codes.to_lookup_ident_list(&mut lookup_ident_list);

    let mut display_list = proc_macro2::TokenStream::new();
    codes.to_display_list(&mut display_list);

    proc_macro::TokenStream::from(quote! {
        impl #name {
            pub fn lookup_ident(s: &str) -> Self {
                match s.to_ascii_uppercase().as_ref() {
                    #lookup_ident_list
                    _ => #name::IDENT,
                }
            }
        }

        impl ::std::fmt::Display for #name {
            fn fmt(&self, f: &mut ::std::fmt::Formatter) -> ::std::fmt::Result {
                write!(
                    f,
                    "{}",
                    match self {
                        #display_list
                    }
                )
            }
        }

        impl ::std::fmt::Debug for #name {
            fn fmt(&self, f: &mut ::std::fmt::Formatter) -> ::std::fmt::Result {
                write!(f, "{}", self)
            }
        }
    })
}
