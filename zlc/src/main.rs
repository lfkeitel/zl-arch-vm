extern crate clap;
extern crate zlsim_proc;

mod compiler;

use std::fs::File;
use std::io::Write;
use std::path::Path;

use clap::{App, Arg};

const ZL_HEADER: &str = "ZL Arch";

fn main() {
    let app = App::new("ZL Compiler")
        .version("0.1.0")
        .author("Lee Keitel")
        .about("Compile a ZL assembly file to srecord format")
        .arg(Arg::with_name("output").short("o").default_value("stdout"))
        .arg(Arg::with_name("INPUT").required(true))
        .get_matches();

    compile_file(
        app.value_of("INPUT").unwrap(),
        app.value_of("output").unwrap(),
    );
}

fn compile_file(path: &str, output: &str) {
    eprintln!("Compiling {}", path);
    let src_path = Path::new(path);
    let code = compiler::compile_file(src_path).unwrap_or_else(|e| {
        eprintln!("{}", e);
        std::process::exit(1);
    });

    let mut total = 0;
    code.iter().for_each(|cs| {
        total += cs.code.len();
        eprintln!("Org: {:04X} - RAM Size: {} bytes", cs.org, cs.code.len());
    });
    eprintln!("Total RAM: {} bytes", total);
    write_code_to_file(&code, output);
}

fn write_code_to_file(code: &[zlsim::CodeSection], output: &str) {
    let mut records = srecord::Srecord(Vec::with_capacity(code.len()));
    records.add_header(ZL_HEADER);

    let mut line_count = 0;

    for part in code {
        let mut total_len = part.code.len();
        let mut pc = part.org;
        let mut i = 0;

        while total_len > 252 {
            records.add_record16(srecord::SrecType::SrecData16, pc, &part.code[i..i + 252]);
            pc += 252;
            i += 252;
            total_len -= 252;
            line_count += 1;
        }

        records.add_record16(srecord::SrecType::SrecData16, pc, &part.code[i..]);
        line_count += 1;
    }

    if line_count < 0xFFFF {
        records.add_record16(srecord::SrecType::SrecCount16, line_count as u16, &[]);
    }
    records.add_record16(srecord::SrecType::SrecStart16, 0, &[]);

    if output == "stdout" {
        print!("{}", records);
    } else if let Ok(file) = File::create(Path::new(output)) {
        match write!(&file, "{}", records) {
            Ok(_) => println!("Compile successful"),
            Err(e) => println!("{}", e),
        }
    } else {
        eprintln!("Unable to open file {}", output);
    }
}
