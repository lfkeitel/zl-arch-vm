use super::{Parser, ParserError};
use crate::compiler::token::TokenType;
use zlsim::opcodes::OpCode;

impl Parser {
    pub(crate) fn parse_no_args(&mut self, c: OpCode) -> Result<(), ParserError> {
        self.prog.append_code(&[c as u8]);
        self.expect_token(TokenType::END_INST)
    }

    pub(crate) fn parse_load(
        &mut self,
        addr: OpCode,
        imm: OpCode,
        rel: OpCode,
    ) -> Result<(), ParserError> {
        self.read_token();
        match self.cur_tok.name {
            TokenType::NUMBER | TokenType::IDENT => {
                let val = self.parse_address(1)?;
                self.prog
                    .append_code(&[addr as u8, (val >> 8) as u8, val as u8]);
            }
            TokenType::IMMEDIATE => {
                self.read_token();
                let val = self.parse_address(1)?;
                self.prog
                    .append_code(&[imm as u8, (val >> 8) as u8, val as u8]);
            }
            TokenType::END_INST => {
                self.prog.append_code(&[rel as u8]);
                return Ok(());
            }
            _ => {
                return Err(self.tokens_err(&[
                    TokenType::NUMBER,
                    TokenType::IDENT,
                    TokenType::IMMEDIATE,
                    TokenType::END_INST,
                ]));
            }
        };

        self.expect_token(TokenType::END_INST)
    }

    pub(crate) fn parse_store(&mut self, addr: OpCode, rel: OpCode) -> Result<(), ParserError> {
        self.read_token();

        match self.cur_tok.name {
            TokenType::NUMBER | TokenType::IDENT => {
                let val = self.parse_address(1)?;
                self.prog
                    .append_code(&[addr as u8, (val >> 8) as u8, val as u8]);
            }
            TokenType::END_INST => {
                self.prog.append_code(&[rel as u8]);
                return Ok(());
            }
            _ => {
                return Err(self.tokens_err(&[
                    TokenType::NUMBER,
                    TokenType::IDENT,
                    TokenType::END_INST,
                ]));
            }
        };

        self.expect_token(TokenType::END_INST)
    }
}
