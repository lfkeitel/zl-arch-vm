mod instructions;
pub mod program;

use std::collections::HashMap;
use std::fmt;
use std::fs::File;
use std::io::{BufReader, Read};
use std::mem;
use std::path::{Path, PathBuf};

use super::lexer::Lexer;
use super::token::{Token, TokenType};

use zlsim::opcodes::OpCode;

use program::*;

pub enum ParserError {
    InvalidCode(String),
    ExpectedToken(String),
    ValidationError(String),
    FileNotFound(String),
}

impl fmt::Display for ParserError {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match self {
            ParserError::InvalidCode(s) => write!(f, "{}", s),
            ParserError::ExpectedToken(s) => write!(f, "{}", s),
            ParserError::ValidationError(s) => write!(f, "{}", s),
            ParserError::FileNotFound(s) => write!(f, "{}", s),
        }
    }
}

impl fmt::Debug for ParserError {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match self {
            ParserError::InvalidCode(s) => write!(f, "{}", s),
            ParserError::ExpectedToken(s) => write!(f, "{}", s),
            ParserError::ValidationError(s) => write!(f, "{}", s),
            ParserError::FileNotFound(s) => write!(f, "{}", s),
        }
    }
}

pub struct Parser {
    lexer: Box<dyn Iterator<Item = Token>>,
    lexer_stack: Vec<Box<dyn Iterator<Item = Token>>>,
    cur_tok: Token,
    peek_tok: Token,
    prog: Program,
    consts: HashMap<String, u16>,
}

impl Parser {
    pub fn new<I: 'static>(mut lexer: I) -> Self
    where
        I: Iterator<Item = Token>,
    {
        let cur = lexer.next().unwrap();
        let peek = lexer.next().unwrap();

        Parser {
            lexer: Box::new(lexer),
            lexer_stack: Vec::new(),
            cur_tok: cur,
            peek_tok: peek,
            prog: Program::new(),
            consts: HashMap::new(),
        }
    }

    pub fn parse(mut self) -> Result<Program, ParserError> {
        while self.cur_tok.name != TokenType::EOF {
            let res: Result<(), ParserError> = match self.cur_tok.name {
                // Skip empty lines
                TokenType::END_INST | TokenType::COMMENT => {
                    self.read_token();
                    continue;
                }
                TokenType::EOF => break,

                // Memory operations
                TokenType::LOAD => self.parse_load(OpCode::LOADA, OpCode::LOADI, OpCode::LOADR),
                TokenType::STR => self.parse_store(OpCode::STOREA, OpCode::STORER),

                // Control, inst stack manipulation
                TokenType::HALT => self.parse_no_args(OpCode::HALT),
                TokenType::NOOP => self.parse_no_args(OpCode::NOOP),
                TokenType::DUP => self.parse_no_args(OpCode::DUP),
                TokenType::DROP => self.parse_no_args(OpCode::DROP),
                TokenType::REV => self.parse_no_args(OpCode::REV),
                TokenType::SRUP => self.parse_no_args(OpCode::SRUP),
                TokenType::SRDN => self.parse_no_args(OpCode::SRDN),
                TokenType::TEST => self.parse_no_args(OpCode::TEST),

                // Program stack
                TokenType::PUSH => self.parse_no_args(OpCode::PUSH),
                TokenType::POP => self.parse_no_args(OpCode::POP),
                TokenType::LOADSP => self.parse_no_args(OpCode::LOADSP),
                TokenType::PUSHSP => self.parse_no_args(OpCode::PUSHSP),

                // Arithmetic and logic
                TokenType::ADD => self.parse_no_args(OpCode::ADD),
                TokenType::SUB => self.parse_no_args(OpCode::SUB),
                TokenType::MUL => self.parse_no_args(OpCode::MUL),
                TokenType::DIV => self.parse_no_args(OpCode::DIV),
                TokenType::MOD => self.parse_no_args(OpCode::MOD),

                TokenType::OR => self.parse_no_args(OpCode::OR),
                TokenType::AND => self.parse_no_args(OpCode::AND),
                TokenType::XOR => self.parse_no_args(OpCode::XOR),
                TokenType::NOT => self.parse_no_args(OpCode::NOT),

                TokenType::ASHR => self.parse_no_args(OpCode::ASHR),
                TokenType::ASHL => self.parse_no_args(OpCode::ASHL),
                TokenType::LSHR => self.parse_no_args(OpCode::LSHR),
                TokenType::LSHL => self.parse_no_args(OpCode::LSHL),

                TokenType::ADDMV => self.parse_no_args(OpCode::ADDMV),
                TokenType::MULMV => self.parse_no_args(OpCode::MULMV),
                TokenType::ANDMV => self.parse_no_args(OpCode::ANDMV),
                TokenType::ORMV => self.parse_no_args(OpCode::ORMV),

                // Branch
                TokenType::JMP => self.parse_no_args(OpCode::JMP),
                TokenType::JMPZ => self.parse_no_args(OpCode::JMPZ),
                TokenType::JMPG => self.parse_no_args(OpCode::JMPG),
                TokenType::JMPL => self.parse_no_args(OpCode::JMPL),
                TokenType::JMPNZ => self.parse_no_args(OpCode::JMPNZ),
                TokenType::CALL => self.parse_no_args(OpCode::CALL),
                TokenType::RET => self.parse_no_args(OpCode::RET),

                TokenType::SWI => self.parse_no_args(OpCode::SWI),
                TokenType::IRET => self.parse_no_args(OpCode::IRET),

                // Meta instructions
                TokenType::DEBUG => self.parse_no_args(OpCode::DEBUG),
                TokenType::LABEL => self.make_label(),
                TokenType::RMB => self.ins_rmb(),
                TokenType::ORG => self.ins_org(),
                TokenType::FCB => self.raw_data_fcb(),
                TokenType::FDB => self.raw_data_fdb(),
                TokenType::DEFINE => self.define_const(),
                TokenType::INCLUDE => self.include_file(),

                _ => Err(ParserError::InvalidCode(format!(
                    "{}: line {}, col {} Unknown token {}",
                    self.cur_tok.file, self.cur_tok.line, self.cur_tok.col, self.cur_tok.name
                ))),
            };

            if let Err(e) = res {
                return Err(e);
            }

            self.read_token()
        }

        match self.prog.validate() {
            Ok(_) => Ok(self.prog),
            Err(e) => Err(ParserError::ValidationError(e)),
        }
    }

    fn read_token(&mut self) {
        self.cur_tok = self.peek_tok.clone();
        self.peek_tok = self.lexer.next().unwrap();

        while self.peek_tok.name == TokenType::COMMENT {
            self.peek_tok = self.lexer.next().unwrap();
        }

        while self.peek_tok.name == TokenType::EOF && !self.lexer_stack.is_empty() {
            self.lexer = self.lexer_stack.pop().unwrap();
            self.peek_tok = self.lexer.next().unwrap();
        }
    }

    // Utility methods
    fn cur_token_is(&self, t: TokenType) -> bool {
        self.cur_tok.name == t
    }

    fn parse_err(&self, msg: &str) -> ParserError {
        ParserError::InvalidCode(format!(
            "{} on line {} in {}",
            msg, self.cur_tok.line, self.cur_tok.file
        ))
    }

    fn token_err(&self, t: TokenType) -> ParserError {
        ParserError::ExpectedToken(format!(
            "expected {} on line {} in {}, got {}",
            t, self.cur_tok.line, self.cur_tok.file, self.cur_tok.name
        ))
    }

    fn tokens_err(&self, t: &[TokenType]) -> ParserError {
        ParserError::ExpectedToken(format!(
            "expected {:?} on line {} in {}, got {}",
            t, self.cur_tok.line, self.cur_tok.file, self.cur_tok.name
        ))
    }

    fn expect_token(&mut self, t: TokenType) -> Result<(), ParserError> {
        self.read_token();
        if !self.cur_token_is(t) {
            Err(self.token_err(t))
        } else {
            Ok(())
        }
    }

    // Meta instructions
    fn include_file(&mut self) -> Result<(), ParserError> {
        self.read_token();

        if !self.cur_token_is(TokenType::STRING) {
            return Err(self.parse_err("INCLUDE requires a file name"));
        }

        let filepath = self.get_include_path()?;
        let file = match File::open(&filepath) {
            Ok(file) => file,
            Err(err) => return Err(ParserError::FileNotFound(format!("{}", err))),
        };

        let buf = BufReader::new(file);
        let reader = buf.bytes();
        let mut new_lexer: Box<dyn Iterator<Item = Token>> =
            Box::new(Lexer::new(reader, filepath.to_str().unwrap_or_default()));

        mem::swap(&mut self.lexer, &mut new_lexer);

        self.lexer_stack.push(new_lexer);

        self.expect_token(TokenType::END_INST)
    }

    fn get_include_path(&self) -> Result<PathBuf, ParserError> {
        let filepath = Path::new(&self.cur_tok.literal);
        if filepath.is_absolute() {
            return Ok(filepath.to_path_buf());
        }

        let cur_filepath = Path::new(&self.cur_tok.file)
            .canonicalize() // Canonicalize current file to get parent later
            .unwrap() // The file exists since we're processing it now, if it doesn't too bad.
            .parent()
            .unwrap_or_else(|| Path::new(""))
            .join(filepath);

        cur_filepath.canonicalize().map_err(|e| {
            self.parse_err(&format!("Failed to include {}, {}", filepath.display(), e))
        })
    }

    fn make_label(&mut self) -> Result<(), ParserError> {
        if self.prog.has_label(&self.cur_tok.literal) {
            Err(self.parse_err(&format!(
                "Duplicate label definition for {}",
                self.cur_tok.literal
            )))
        } else {
            self.prog.add_label(&self.cur_tok.literal);
            Ok(())
        }
    }

    fn raw_data_fcb(&mut self) -> Result<(), ParserError> {
        self.read_token();

        loop {
            if !self.cur_token_is(TokenType::NUMBER) && !self.cur_token_is(TokenType::STRING) {
                return Err(self.parse_err("Constant byte must be a number or string"));
            }

            if self.cur_token_is(TokenType::STRING) {
                self.prog.append_code(self.cur_tok.literal.as_bytes());
            } else if let Some(val) = parse_u16(&self.cur_tok.literal) {
                if val <= 255 {
                    self.prog.append_code(&[val as u8]);
                } else {
                    return Err(self.parse_err("Invalid byte"));
                }
            } else {
                return Err(self.parse_err("Invalid byte"));
            }

            self.read_token();
            if self.cur_token_is(TokenType::END_INST) {
                break;
            }

            if !self.cur_token_is(TokenType::COMMA) {
                return Err(self.token_err(TokenType::COMMA));
            }
            self.read_token();
        }

        Ok(())
    }

    fn raw_data_fdb(&mut self) -> Result<(), ParserError> {
        self.read_token();

        loop {
            if self.cur_token_is(TokenType::STRING) {
                for b in self.cur_tok.literal.as_bytes() {
                    self.prog.append_code(&[0, *b]);
                }
            } else {
                let val = self.parse_address(0)?;
                self.prog.append_code(&[(val >> 8) as u8, val as u8]);
            }

            self.read_token();
            if self.cur_token_is(TokenType::END_INST) {
                break;
            }

            if !self.cur_token_is(TokenType::COMMA) {
                return Err(self.token_err(TokenType::COMMA));
            }
            self.read_token();
        }

        Ok(())
    }

    fn ins_rmb(&mut self) -> Result<(), ParserError> {
        self.read_token();
        if !self.cur_token_is(TokenType::NUMBER) {
            return Err(self.token_err(TokenType::NUMBER));
        }

        if let Some(val) = parse_u16(&self.cur_tok.literal) {
            let buf: Vec<u8> = vec![0; val as usize];
            self.prog.append_code(&buf);
            Ok(())
        } else {
            Err(self.parse_err("invalid number literal"))
        }
    }

    fn ins_org(&mut self) -> Result<(), ParserError> {
        self.read_token();
        let val = self.parse_address(0)?;
        if val != self.prog.org() {
            self.prog.add_code_part(val);
        }
        Ok(())
    }

    fn define_const(&mut self) -> Result<(), ParserError> {
        self.read_token();
        if !self.cur_token_is(TokenType::IDENT) {
            return Err(self.token_err(TokenType::IDENT));
        }

        let name = self.cur_tok.literal.clone();

        self.read_token();

        match self.cur_tok.name {
            TokenType::NUMBER => {
                if let Some(val) = parse_u16(&self.cur_tok.literal) {
                    self.consts.insert(name, val);
                    Ok(())
                } else {
                    Err(self.parse_err("invalid number literal"))
                }
            }
            TokenType::STRING => {
                let bytes = self.cur_tok.literal.as_bytes();
                match bytes.len() {
                    0 => Err(self.parse_err("Zero length string constant")),
                    1 => {
                        self.consts.insert(name, u16::from(bytes[0]));
                        Ok(())
                    }
                    _ => {
                        println!(
                            "WARNING on line {} in {}: Constant string is longer than one byte, using first byte only",
                            self.cur_tok.line, self.cur_tok.file
                        );
                        self.consts.insert(name, u16::from(bytes[0]));
                        Ok(())
                    }
                }
            }
            _ => Err(self.tokens_err(&[TokenType::NUMBER, TokenType::STRING])),
        }
    }

    // Argument parser methods
    fn parse_address(&mut self, pcoffset: u16) -> Result<u16, ParserError> {
        match self.cur_tok.name {
            TokenType::NUMBER => match parse_u16(&self.cur_tok.literal) {
                Some(n) => Ok(n),
                None => Err(self.parse_err("invalid address")),
            },
            TokenType::STRING => {
                let bytes = self.cur_tok.literal.as_bytes();

                match bytes.len() {
                    0 => Ok(0),
                    1 => Ok(u16::from(bytes[0])),
                    2 => Ok((u16::from(bytes[0]) << 8) + u16::from(bytes[1])),
                    _ => Err(self.parse_err("string too long")),
                }
            }
            TokenType::IDENT => {
                let mut label: &str = &self.cur_tok.literal.as_ref();
                let lit = label;

                let mut offset = 0i16;

                let add_index = label.find('+').unwrap_or_default();
                let sub_index = label.find('-').unwrap_or_default();

                if add_index > 0 || sub_index > 0 {
                    let ind = {
                        if add_index > 0 {
                            add_index
                        } else {
                            sub_index
                        }
                    } as usize;

                    label = lit.split_at(ind).0;
                    let x: &[_] = &['-', '+'];
                    offset = match parse_u16(lit.split_at(ind).1.trim_start_matches(x)) {
                        Some(n) => n,
                        None => return Err(self.parse_err("invalid address offset")),
                    } as i16;

                    if ind == sub_index {
                        offset = -offset
                    }
                }

                if label == "$" {
                    Ok(((self.prog.pc() as i16) + offset) as u16)
                } else if let Some(val) = self.consts.get(label) {
                    Ok(((*val as i16) + offset) as u16)
                } else {
                    self.prog.add_link(
                        pcoffset,
                        label,
                        offset,
                        self.cur_tok.line,
                        &self.cur_tok.file,
                    );
                    Ok(0)
                }
            }
            _ => Err(self.parse_err(&format!("invalid address type {}", self.cur_tok.name))),
        }
    }
}

fn parse_u16(s: &str) -> Option<u16> {
    if s.starts_with('!') {
        match u16::from_str_radix(s.trim_start_matches('!'), 2) {
            Ok(n) => Some(n),
            Err(_) => None,
        }
    } else if s.starts_with("0x") {
        match u16::from_str_radix(s.trim_start_matches("0x"), 16) {
            Ok(n) => Some(n),
            Err(_) => None,
        }
    } else {
        match s.parse::<u16>() {
            Ok(n) => Some(n),
            Err(_) => None,
        }
    }
}
