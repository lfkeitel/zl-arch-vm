mod lexer;
mod linker;
mod parser;
mod token;

use std::fs::File;
use std::io::{BufReader, Read};
use std::path::Path;

use parser::{Parser, ParserError};
use zlsim::Code;

pub fn compile_file(filepath: &Path) -> Result<Code, ParserError> {
    let file = match File::open(filepath) {
        Ok(file) => file,
        Err(err) => return Err(ParserError::FileNotFound(format!("{}", err))),
    };

    let buf = BufReader::new(file);
    let reader = buf.bytes();
    let lex = lexer::Lexer::new(reader, filepath.to_str().unwrap_or_default());
    let mut prog = Parser::new(lex).parse()?;

    if let Err(s) = linker::link(&mut prog) {
        Err(ParserError::InvalidCode(s))
    } else {
        Ok(prog.to_code())
    }
}
