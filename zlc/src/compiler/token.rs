#![allow(clippy::upper_case_acronyms)]

use zlsim_proc::TokenExtras;

#[allow(non_camel_case_types)]
#[derive(Copy, Clone, PartialEq, TokenExtras)]
pub enum TokenType {
    #[not_keyword]
    ILLEGAL,
    #[not_keyword]
    EOF,
    #[not_keyword]
    COMMENT,
    #[not_keyword]
    END_INST,
    #[not_keyword]
    COMMA,
    #[not_keyword]
    IMMEDIATE,
    #[not_keyword]
    IDENT,
    #[not_keyword]
    LABEL,
    #[not_keyword]
    NUMBER,
    #[not_keyword]
    STRING,

    NOOP,
    HALT,

    LOAD,
    STR,

    ADD,
    SUB,
    MUL,
    DIV,
    MOD,

    OR,
    AND,
    XOR,
    NOT,

    ASHR,
    ASHL,
    LSHR,
    LSHL,

    ADDMV,
    MULMV,
    ANDMV,
    ORMV,

    DUP,
    DROP,
    TEST,
    REV,
    SRUP,
    SRDN,

    PUSH,
    POP,
    LOADSP,
    PUSHSP,

    JMP,
    JMPZ,
    JMPG,
    JMPL,
    JMPNZ,
    CALL,
    RET,

    SWI,
    IRET,

    RMB,
    ORG,
    FCB,
    FDB,
    DEBUG,
    DEFINE,
    INCLUDE,
}

#[derive(Clone, Debug)]
pub struct Token {
    pub name: TokenType,
    pub literal: String,
    pub line: u32,
    pub col: u32,
    pub file: String,
}

impl Token {
    pub fn with_literal(t: TokenType, lit: String, line: u32, col: u32, file: &str) -> Self {
        Token {
            name: t,
            literal: lit,
            line,
            col,
            file: file.to_owned(),
        }
    }

    pub fn simple(t: TokenType, line: u32, col: u32, file: &str) -> Self {
        Self::with_literal(t, "".to_string(), line, col, file)
    }
}
