extern crate clap;

use std::path::Path;

use clap::{App, Arg};

use srecord::SrecType;
use zlsim_vm::io_devices;
use zlsim_vm::memory::Memory;
use zlsim_vm::{IODevice, Port, VM};

const ZL_HEADER: &str = "ZL Arch";
const NUM_OF_MEMORY_CELLS: usize = 65536;

fn main() {
    let app = App::new("ZL Compiler")
        .version("0.1.0")
        .author("Lee Keitel")
        .about("Compile and execute a ZL assembly file")
        .arg(Arg::with_name("INPUT").required(true))
        .get_matches();

    exec_srecord(app.value_of("INPUT").unwrap());
}

fn exec_srecord(path: &str) {
    let srec_path = Path::new(path);

    let records = srecord::parse_file(srec_path).unwrap_or_else(|e| {
        eprintln!("{}", e);
        std::process::exit(1);
    });

    let mut code = Vec::new();

    let header = records.0.first().unwrap();
    if header.data != ZL_HEADER.as_bytes().to_vec() {
        eprintln!("Srecord is not a ZL binary");
        return;
    }

    for r in records.0 {
        if r.rec_type == SrecType::SrecData16 {
            code.push(zlsim_vm::CodeSection {
                org: r.address as u16,
                code: r.data,
            });
        }
    }

    execute_code(&code);
}

fn execute_code(code: &[zlsim_vm::CodeSection]) {
    let memory = Memory::with_code(NUM_OF_MEMORY_CELLS, code);
    let mut vm = zlsim_vm::VM::new(memory);

    setup_clock_device(&mut vm);
    setup_keyboard_device(&mut vm);
    setup_line_display_device(&mut vm);

    vm.reset();
    vm.run().unwrap();
}

fn setup_clock_device(vm: &mut VM) {
    const CLOCK_COMMAND_PORT_ADDR: u16 = 0x02A6;
    const CLOCK_DATA_PORT_ADDR: u16 = 0x02A8;

    vm.install_port(IODevice {
        address: CLOCK_COMMAND_PORT_ADDR,
        device: Port::Command(Box::new(io_devices::clock::Clock::new(
            CLOCK_DATA_PORT_ADDR,
        ))),
    });
}

fn setup_keyboard_device(vm: &mut VM) {
    const KEYBOARD_COMMAND_PORT_ADDR: u16 = 0x02A2;
    const KEYBOARD_DATA_PORT_ADDR: u16 = 0x02A4;

    vm.install_port(IODevice {
        address: KEYBOARD_COMMAND_PORT_ADDR,
        device: Port::Command(Box::new(io_devices::keyboard::Keyboard::new(
            KEYBOARD_DATA_PORT_ADDR,
        ))),
    });
}

fn setup_line_display_device(vm: &mut VM) {
    const BUFFER_START: u16 = 0x0200;
    const BUFFER_END: u16 = 0x029f;
    const DISPLAY_COMMAND_PORT_ADDR: u16 = 0x02A0;

    vm.install_port(IODevice {
        address: DISPLAY_COMMAND_PORT_ADDR,
        device: Port::Command(Box::new(io_devices::line_display::LineDisplay::new(
            BUFFER_START,
            BUFFER_END,
        ))),
    });
}
