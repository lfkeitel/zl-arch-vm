#!/bin/bash
set -e

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"

TMP_OUT="$(mktemp)"

echo "Temp file: $TMP_OUT"

"$DIR/target/debug/zlc" -o "$TMP_OUT" "$1"
"$DIR/target/debug/zlsim" "$TMP_OUT"

rm "$TMP_OUT"
