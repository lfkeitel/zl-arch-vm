use std::time::{SystemTime, UNIX_EPOCH};

use crate::{CmdPort, VMState};

const COMMAND_GET_TIME: u16 = 0x10;

pub struct Clock {
    data_port: u16,
}

impl Clock {
    pub fn new(data_port: u16) -> Self {
        Clock{data_port}
    }
}

impl CmdPort for Clock {
    fn command_port(&mut self, vm: &mut VMState, data: u16) {
        if data == COMMAND_GET_TIME {
            let start = SystemTime::now();
            let since_the_epoch = start
                .duration_since(UNIX_EPOCH)
                .expect("Time went backwards")
                .as_secs();

            vm.write_mem_u16(self.data_port, (since_the_epoch >> 48) as u16);
            vm.write_mem_u16(self.data_port + 2, (since_the_epoch >> 32) as u16);
            vm.write_mem_u16(self.data_port + 4, (since_the_epoch >> 16) as u16);
            vm.write_mem_u16(self.data_port + 6, since_the_epoch as u16);
        }
    }
}
