use std::io;
use std::io::prelude::*;

use crate::{CmdPort, VMState};

const COMMAND_DISPLAY: u16 = 0x10;
const COMMAND_DISPLAY_GRID: u16 = 0x11;
const COMMAND_NEWLINE: u16 = 0x15;
const COMMAND_CLEAR: u16 = 0x16;

pub struct LineDisplay {
    buffer_start: u16,
    buffer_end: u16,
}

impl LineDisplay {
    pub fn new(buffer_start: u16, buffer_end: u16) -> Self {
        LineDisplay {
            buffer_start,
            buffer_end,
        }
    }
}

impl CmdPort for LineDisplay {
    fn command_port(&mut self, vm: &mut VMState, data: u16) {
        match data {
            COMMAND_DISPLAY => {
                let mut line = String::with_capacity(80);
                for loc in 0..80 {
                    line.push(
                        vm.memory
                            .read_u8((self.buffer_start + (loc * 2) + 1) as usize)
                            as char,
                    );
                }

                print!("\r\x1B[2K{}", line); // /r go to start of line, \x1B[2K clear full line
                io::stdout().flush().unwrap();
            }
            COMMAND_DISPLAY_GRID => {
                let grid_buffer_addr = vm.read_mem_u16(self.buffer_start);
                let mut grid = String::with_capacity((80 * 25) + (2 * 25)); // 80 cols, 25 rows, 2 per row for \r\n

                for row in 0..25 {
                    for col in 0..80 {
                        let offset = ((row * 80) + col) * 2; // Offset to word
                        grid.push(
                            vm.memory.read_u8((grid_buffer_addr + offset + 1) as usize) as char
                        );
                    }
                    if row < 24 {
                        grid.push_str("\r\n");
                    }
                }

                print!("\x1B[2J\r\n{}", grid); // \x1B[2J clear full screen
                io::stdout().flush().unwrap();
            }
            COMMAND_NEWLINE => {
                println!();
                // Clear buffer
                for loc in self.buffer_start..=self.buffer_end {
                    vm.memory.write_u8(loc as usize, 0);
                }
            }
            COMMAND_CLEAR => {
                // Clear buffer
                for loc in self.buffer_start..=self.buffer_end {
                    vm.memory.write_u8(loc as usize, 0);
                }
            }
            _ => {}
        }
    }
}
