use std::io::{stdin, stdout};

use crate::{CmdPort, VMState};

use termion::event::Key;
use termion::input::TermRead;
use termion::raw::IntoRawMode;

const COMMAND_GET_KEY: u16 = 0x10;

const KEYCODE_INVALID: u8 = 0;

// Standard ASCII range
const KEYCODE_CTRL_D: u8 = 0x04;
const KEYCODE_BACKSPACE: u8 = 0x08;
const KEYCODE_CTRL_Z: u8 = 0x1A;
const KEYCODE_ESCAPE: u8 = 0x1B;
const KEYCODE_DELETE: u8 = 0x7F;

// Extended ASCII range
const KEYCODE_LEFT: u8 = 255;
const KEYCODE_RIGHT: u8 = 254;
const KEYCODE_UP: u8 = 253;
const KEYCODE_DOWN: u8 = 252;
const KEYCODE_F12: u8 = 251;
const KEYCODE_F11: u8 = 250;
const KEYCODE_F10: u8 = 249;
const KEYCODE_F9: u8 = 248;
const KEYCODE_F8: u8 = 247;
const KEYCODE_F7: u8 = 246;
const KEYCODE_F6: u8 = 245;
const KEYCODE_F5: u8 = 244;
const KEYCODE_F4: u8 = 243;
const KEYCODE_F3: u8 = 242;
const KEYCODE_F2: u8 = 241;
const KEYCODE_F1: u8 = 240;
const KEYCODE_PAGEUP: u8 = 239;
const KEYCODE_PAGEDOWN: u8 = 238;

pub struct Keyboard {
    data_port: u16,
}

impl Keyboard {
    pub fn new(data_port: u16) -> Self {
        Keyboard { data_port }
    }
}

impl CmdPort for Keyboard {
    fn command_port(&mut self, vm: &mut VMState, data: u16) {
        if data == COMMAND_GET_KEY {
            let _stdout = stdout().into_raw_mode().unwrap(); // Enables raw mode for input

            let keycode = match stdin().keys().next() {
                Some(key) => match key.unwrap() {
                    Key::Char(c) => c as u8,
                    Key::Esc => KEYCODE_ESCAPE,
                    Key::Delete => KEYCODE_DELETE,
                    Key::Backspace => KEYCODE_BACKSPACE,
                    Key::Ctrl(c) => match c {
                        'd' => KEYCODE_CTRL_D,
                        'z' => KEYCODE_CTRL_Z,
                        _ => KEYCODE_INVALID,
                    },
                    Key::F(i) => match i {
                        1 => KEYCODE_F1,
                        2 => KEYCODE_F2,
                        3 => KEYCODE_F3,
                        4 => KEYCODE_F4,
                        5 => KEYCODE_F5,
                        6 => KEYCODE_F6,
                        7 => KEYCODE_F7,
                        8 => KEYCODE_F8,
                        9 => KEYCODE_F9,
                        10 => KEYCODE_F10,
                        11 => KEYCODE_F11,
                        12 => KEYCODE_F12,
                        _ => KEYCODE_INVALID,
                    },
                    Key::PageDown => KEYCODE_PAGEDOWN,
                    Key::PageUp => KEYCODE_PAGEUP,
                    Key::Left => KEYCODE_LEFT,
                    Key::Right => KEYCODE_RIGHT,
                    Key::Up => KEYCODE_UP,
                    Key::Down => KEYCODE_DOWN,
                    _ => KEYCODE_INVALID,
                },
                None => KEYCODE_INVALID,
            };

            vm.memory.write_u8((self.data_port + 1) as usize, keycode);
        }
    }
}
