#![allow(clippy::upper_case_acronyms)]

use zlsim_proc::OpcodeExtras;

#[repr(u8)]
#[derive(PartialEq, OpcodeExtras)]
pub enum OpCode {
    #[default]
    NOOP = 0x00,
    DEBUG = 0x01,

    LOADS = 0x15,
    STORES = 0x16,
    LOADI = 0x19, // Takes 2 byte arg
    LOADA = 0x1A, // Takes 2 byte arg
    LOADR = 0x1B,
    DUP = 0x1C,
    STOREA = 0x1D, // Takes 2 byte arg
    STORER = 0x1E,
    DROP = 0x1F,

    PUSH = 0x20,
    POP = 0x21,
    LOADSP = 0x22,
    PUSHSP = 0x23,

    ADD = 0x50,
    SUB = 0x51,
    MUL = 0x52,
    DIV = 0x53,
    MOD = 0x54,

    AND = 0x60,
    OR = 0x61,
    XOR = 0x62,
    NOT = 0x63,

    ASHR = 0x64,
    ASHL = 0x65,
    LSHR = 0x66,
    LSHL = 0x67,

    ADDMV = 0x70,
    MULMV = 0x72,

    ANDMV = 0x80,
    ORMV = 0x81,

    JMP = 0xA0,
    JMPZ = 0xA1,
    JMPG = 0xA2,
    JMPL = 0xA3,
    JMPNZ = 0xA4,
    CALL = 0xA5,
    RET = 0xA6,

    HALT = 0xF0,
    SWI = 0xF1,
    IRET = 0xF2,
    TEST = 0xF6,
    REV = 0xF8,
    SRUP = 0xF9,
    SRDN = 0xFA,
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn test_opcode_convert() {
        let i: u8 = 1;
        let op = OpCode::from(i);

        assert_eq!(op, OpCode::DEBUG);
    }

    #[test]
    fn test_invalid_opcode_value() {
        let i: u8 = 255;
        let op = OpCode::from(i);

        assert_eq!(op, OpCode::NOOP);
    }
}
