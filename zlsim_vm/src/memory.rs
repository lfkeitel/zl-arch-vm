use super::CodeSection;

#[derive(Default)]
pub struct Memory ( Vec<u8> );

impl Memory {
    pub fn new(size: usize) -> Self {
        Memory(vec![0; size])
    }

    pub fn with_code(size: usize, code: &[CodeSection]) -> Self {
        let mut memory = Memory::new(size);
        for section in code {
            let pc = section.org;

            for (i, b) in section.code.iter().enumerate() {
                let loc = i as u16 + pc;

                memory.0[loc as usize] = b.to_owned();
            }
        }
        memory
    }

    pub fn read_u8(&self, addr: usize) -> u8 {
        self.0[addr]
    }

    pub fn read_u16(&self, addr: usize) -> u16 {
        let b1 = u16::from(self.0[addr]);
        let b2 = u16::from(self.0[addr.wrapping_add(1)]);
        b1 << 8 | b2
    }

    pub fn write_u8(&mut self, addr: usize, data: u8) {
        self.0[addr] = data;
    }

    pub fn write_u16(&mut self, addr: usize, data: u16) {
        self.0[addr] = (data >> 8) as u8;
        self.0[addr.wrapping_add(1)] = data as u8;
    }
}
