#![allow(clippy::upper_case_acronyms)]

pub mod io_devices;
pub mod memory;
pub mod opcodes;

use std::collections::HashMap;
use std::io;
use std::process;

use crate::opcodes::OpCode as opc;
use memory::Memory;

// VM size constants
const SIZE_OF_INST_STACK: usize = 100;

// Interrupt addresses
const INRPT_RESET: u16 = 0x0000;
const INRPT_SOFTWARE: u16 = 0x0002;

// Memory area boundaries
const MEM_AREA_RO_START: u16 = 0x1000;
const MEM_AREA_RO_END: u16 = 0x1FFF;

const MEM_AREA_USER_START: u16 = 0x2000;
const MEM_AREA_USER_END: u16 = 0xEFFF;

pub type Code = Vec<CodeSection>;

pub struct CodeSection {
    pub org: u16,
    pub code: Vec<u8>,
}

pub struct IODevice {
    pub address: u16,
    pub device: Port,
}

pub enum Port {
    Command(Box<dyn CmdPort>),
    Data(Box<dyn DataPort>),
}

/// CmdPort allows sending data to an IO module.
pub trait CmdPort {
    fn command_port(&mut self, vm: &mut VMState, data: u16);
}

/// DataPort allows retrieving data from an IO module.
pub trait DataPort {
    fn data_port(&mut self, vm: &mut VMState) -> u16;
}

#[derive(Default)]
struct StatusRegister {
    zero: bool,
    negative: bool,
    overflow: bool,
}

impl StatusRegister {
    fn from_u16(bits: u16) -> Self {
        StatusRegister {
            zero: bits & 0b0000_0000_0000_0001 == 0b0000_0000_0000_0001,
            negative: bits & 0b0000_0000_0000_0010 == 0b0000_0000_0000_0010,
            overflow: bits & 0b0000_0000_0000_0100 == 0b0000_0000_0000_0100,
        }
    }

    fn as_u16(&self) -> u16 {
        let mut bits = 0;
        if self.zero {
            bits |= 0b0000_0000_0000_0001;
        }
        if self.negative {
            bits |= 0b0000_0000_0000_0010;
        }
        if self.overflow {
            bits |= 0b0000_0000_0000_0100;
        }
        bits
    }
}

#[derive(Default)]
pub struct VMState {
    stack: Vec<u16>, // Hardware instruction stack
    memory: Memory,  // Main memory (RAM)
    pc: u16,         // Program Counter
    sp: u16,         // Software stack pointer
    isp: u16,        // Instruction stack pointer
    debug_mode: bool,
    status: StatusRegister,
}

impl VMState {
    pub fn new(memory: Memory) -> VMState {
        VMState {
            stack: vec![0; SIZE_OF_INST_STACK],
            memory,
            pc: 0,
            sp: 0,
            isp: 0,
            debug_mode: false,
            status: StatusRegister::default(),
        }
    }

    fn fetch_u16(&mut self) -> u16 {
        let b1 = u16::from(self.fetch_byte());
        let b2 = u16::from(self.fetch_byte());
        (b1 << 8) | b2
    }

    fn read_mem_u16(&self, addr: u16) -> u16 {
        self.memory.read_u16(addr as usize)
    }

    fn write_mem_u16(&mut self, addr: u16, data: u16) {
        self.memory.write_u16(addr as usize, data)
    }

    // Stack manipulation
    fn pop_stack(&mut self) -> u16 {
        if self.isp < 1 {
            panic!("Instruction stack underflow");
        }

        self.isp -= 1;
        self.stack[(self.isp) as usize]
    }

    fn push_stack(&mut self, item: u16) {
        if self.isp == (SIZE_OF_INST_STACK - 1) as u16 {
            panic!("Instruction stack overflow");
        }

        self.stack[(self.isp) as usize] = item;
        self.isp += 1;
    }

    pub fn reset(&mut self) {
        self.pc = self.read_mem_u16(INRPT_RESET);
    }

    fn fetch_byte(&mut self) -> u8 {
        let b = self.memory.read_u8(self.pc as usize);
        self.pc += 1;
        b
    }
}

#[derive(Default)]
pub struct VM {
    state: VMState,
    ports: HashMap<u16, Port>, // in/out data ports
}

macro_rules! instr_two_arg_signed {
    ($fnname:ident, $oper:ident) => {
        fn $fnname(&mut self) {
            let val1 = self.state.pop_stack() as i16;
            let val2 = self.state.pop_stack() as i16;
            let res = val2.$oper(val1);
            self.state.status.zero = res == 0;
            self.state.status.negative = res < 0;
            self.state.push_stack(res as u16);
        }
    };
}

macro_rules! instr_two_arg_unsigned {
    ($fnname:ident, $oper:tt) => {
        fn $fnname(&mut self) {
            let val1 = self.state.pop_stack();
            let val2 = self.state.pop_stack();
            let res = val2 $oper val1;
            self.state.status.zero = res == 0;
            self.state.status.negative = (res as i16) < 0;
            self.state.push_stack(res as u16);
        }
    };
}

macro_rules! instr_two_arg_unsigned_wrap {
    ($fnname:ident, $oper:ident) => {
        fn $fnname(&mut self) {
            let val1 = self.state.pop_stack() as i16;
            let val2 = self.state.pop_stack() as i16;
            let res = val2.$oper(val1);
            self.state.status.zero = res == 0;
            self.state.status.negative = res < 0;
            self.state.push_stack(res as u16);
        }
    };
}

macro_rules! instr_vectored {
    ($fnname:ident, $oper:tt) => {
        fn $fnname(&mut self) {
            let length = self.state.pop_stack();
            let operand = self.state.pop_stack();
            for i in 1..=length {
                let idx = (self.state.isp - i) as usize;
                self.state.stack[idx] $oper operand;
            }
        }
    };
}

impl VM {
    pub fn new(memory: Memory) -> VM {
        VM {
            state: VMState::new(memory),
            ports: HashMap::new(),
        }
    }

    pub fn install_port(&mut self, device: IODevice) {
        self.ports.insert(device.address, device.device);
    }

    pub fn reset(&mut self) {
        self.state.reset();
    }

    fn print_memory(&self, start: u16) {
        print!("Memory   00 01 02 03 04 05 06 07 08 09 0A 0B 0C 0D 0E 0F  10 11 12 13 14 15 16 17 18 19 1A 1B 1C 1D 1E 1F\n\n");
        let mut i = if (start as usize) + 256 > 0xFFFF {
            0xFFFF - 256
        } else {
            start as usize
        };

        let end = i + 256;

        while i < end {
            print!("{:04X}", i);
            print!("     ");

            for j in 0..16 {
                print!("{:02X}", self.state.memory.read_u8(i + j));
                print!(" ");
            }
            print!(" ");

            for j in 16..32 {
                print!("{:02X}", self.state.memory.read_u8(i + j));
                print!(" ");
            }
            println!();
            i += 32
        }
    }

    fn print_registers(&self) {
        println!(
            "PC: 0x{:04X} | SP: 0x{:04X} | ISP: 0x{:04X}",
            self.state.pc, self.state.sp, self.state.isp
        );
        println!(
            "Z: {} | N: {} | O: {}",
            self.state.status.zero, self.state.status.negative, self.state.status.overflow,
        );
    }

    fn print_stack(&self) {
        if self.state.isp == 0 {
            println!("Stack empty");
            return;
        }

        let mut isp = (self.state.isp) as usize;
        println!("{} - 0x{:04X} <- TOS", isp - 1, self.state.stack[isp - 1]);
        isp -= 1;

        while isp != 0 {
            println!("{} - 0x{:04X}", isp - 1, self.state.stack[isp - 1]);
            isp -= 1;
        }
    }

    pub fn run(&mut self) -> Result<(), String> {
        macro_rules! instruction {
            ($inst:ident) => {{
                self.$inst();
            }};

            ($inst:ident, u8) => {{
                let arg1 = self.state.fetch_byte();
                self.$inst(arg1);
            }};

            ($inst:ident, u16) => {{
                let arg1 = self.state.fetch_u16();
                self.$inst(arg1);
            }};
        }

        let mut debug_disabled = false;

        loop {
            if self.state.pc < MEM_AREA_USER_START || self.state.pc > MEM_AREA_USER_END {
                panic!(
                    "==> attempted execution of non-user memory 0x{:04X} <==",
                    self.state.pc
                );
            }

            let opcode = opc::from(self.state.fetch_byte());

            if self.state.debug_mode {
                println!("Breakpoint hit at 0x{:02X}: {}", self.state.pc - 1, opcode);

                loop {
                    print!("Debug> ");
                    io::Write::flush(&mut io::stdout()).expect("flush failed!");

                    let mut input = String::new();
                    io::stdin().read_line(&mut input).unwrap_or_default();
                    let parts: Vec<&str> = input.trim().split_whitespace().collect();
                    if parts.is_empty() {
                        continue;
                    }

                    match parts[0] {
                        "step" => break,
                        "memory" | "mem" => {
                            if parts.len() == 1 {
                                println!("(memory|mem) ADDRESS");
                                continue;
                            }

                            let address_str = if parts[1].starts_with("0x") {
                                parts[1].trim_start_matches("0x")
                            } else if parts[1].starts_with('x') {
                                parts[1].trim_start_matches('x')
                            } else {
                                parts[1]
                            };

                            let address = u16::from_str_radix(address_str, 16).unwrap_or_default();

                            println!(
                                "[{:04X}-{:04X}] = 0x{:02X}{:02X}",
                                address,
                                address + 1,
                                self.state.memory.read_u8(address as usize),
                                self.state.memory.read_u8((address + 1) as usize)
                            );
                        }
                        "memoryblock" | "memb" => {
                            if parts.len() == 1 {
                                self.print_memory(0);
                                continue;
                            }

                            let address_str = if parts[1].starts_with("0x") {
                                parts[1].trim_start_matches("0x")
                            } else if parts[1].starts_with('x') {
                                parts[1].trim_start_matches('x')
                            } else {
                                parts[1]
                            };

                            let address = u16::from_str_radix(address_str, 16).unwrap_or_default();

                            self.print_memory(address);
                        }
                        "continue" | "cont" => {
                            self.state.debug_mode = false;
                            break;
                        }
                        "disable" | "dis" => debug_disabled = true,
                        "enable" | "en" => debug_disabled = false,
                        "next" => println!("Next Instruction: {:?}", opcode),
                        "registers" | "reg" => self.print_registers(),
                        "stack" => self.print_stack(),
                        "exit" | "quit" => process::exit(0),
                        _ => println!("Unknown command `{}`", parts[0]),
                    }
                }
            }

            match opcode {
                opc::LOADI => instruction!(inst_loadid, u16),
                opc::LOADA => instruction!(inst_loadad, u16),
                opc::LOADR => instruction!(inst_loadrd),

                opc::STOREA => instruction!(inst_storead, u16),
                opc::STORER => instruction!(inst_storerd),

                opc::ADD => instruction!(inst_add),
                opc::SUB => instruction!(inst_sub),
                opc::MUL => instruction!(inst_mul),
                opc::DIV => instruction!(inst_div),
                opc::MOD => instruction!(inst_mod),

                opc::AND => instruction!(inst_and),
                opc::OR => instruction!(inst_or),
                opc::XOR => instruction!(inst_xor),
                opc::NOT => instruction!(inst_bit_not),

                opc::ASHR => instruction!(inst_ashr),
                opc::ASHL => instruction!(inst_ashl),
                opc::LSHR => instruction!(inst_lshr),
                opc::LSHL => instruction!(inst_lshl),

                opc::ADDMV => instruction!(inst_addmv),
                opc::MULMV => instruction!(inst_mulmv),
                opc::ANDMV => instruction!(inst_andmv),
                opc::ORMV => instruction!(inst_ormv),

                opc::DUP => instruction!(inst_dupd),
                opc::DROP => instruction!(inst_dropd),
                opc::REV => instruction!(inst_revd),
                opc::SRUP => instruction!(inst_srup),
                opc::SRDN => instruction!(inst_srdn),
                opc::TEST => instruction!(inst_test),

                opc::LOADSP => instruction!(inst_loadsp),
                opc::PUSHSP => instruction!(inst_pushsp),
                opc::PUSH => instruction!(inst_push),
                opc::POP => instruction!(inst_pop),

                opc::JMP => instruction!(inst_jmp),
                opc::JMPZ => instruction!(inst_jmpz),
                opc::JMPG => instruction!(inst_jmpg),
                opc::JMPL => instruction!(inst_jmpl),
                opc::JMPNZ => instruction!(inst_jmpnz),
                opc::CALL => instruction!(inst_call),
                opc::RET => instruction!(inst_ret),

                opc::SWI => instruction!(inst_swi),
                opc::IRET => instruction!(inst_iret),

                opc::HALT => break,
                opc::NOOP => {}

                opc::DEBUG => {
                    if !debug_disabled {
                        self.state.debug_mode = true
                    }
                }
                _ => return Err(format!("Unknown opcode encountered: {}", opcode)),
            }
        }

        Ok(())
    }

    // Memory manipulation
    fn check_ro_mem_addr(addr: u16) {
        if (MEM_AREA_RO_START..=MEM_AREA_RO_END).contains(&addr) {
            panic!(
                "==> attempted write to read-only memory segment 0x{:04X} <==",
                addr
            )
        }
    }

    fn read_mem_u16(&mut self, addr: u16) -> u16 {
        if let Some(Port::Data(port)) = self.ports.get_mut(&addr) {
            return port.data_port(&mut self.state);
        }

        self.state.read_mem_u16(addr)
    }

    fn write_mem_u16(&mut self, addr: u16, data: u16) {
        if let Some(Port::Command(port)) = self.ports.get_mut(&addr) {
            port.command_port(&mut self.state, data);
            return;
        }

        VM::check_ro_mem_addr(addr);
        self.state.write_mem_u16(addr, data);
    }

    fn push_program_stack(&mut self, data: u16) {
        self.write_mem_u16(self.state.sp.wrapping_sub(1), data);
        self.state.sp = self.state.sp.wrapping_sub(2);
    }

    fn pop_program_stack(&mut self) -> u16 {
        self.state.sp = self.state.sp.wrapping_add(2);
        self.read_mem_u16(self.state.sp.wrapping_sub(1))
    }

    // Instructions

    // LOAD
    fn inst_loadid(&mut self, data: u16) {
        self.state.push_stack(data);
    }

    fn inst_loadad(&mut self, addr: u16) {
        let data = self.read_mem_u16(addr);
        self.state.push_stack(data);
    }

    fn inst_loadrd(&mut self) {
        let addr = self.state.pop_stack();
        self.inst_loadad(addr);
    }

    // STORE
    fn inst_storead(&mut self, addr: u16) {
        let data = self.state.pop_stack();
        self.write_mem_u16(addr, data);
    }

    fn inst_storerd(&mut self) {
        let data = self.state.pop_stack();
        let addr = self.state.pop_stack();
        self.write_mem_u16(addr, data);
    }

    // Misc load/store
    fn inst_dupd(&mut self) {
        let tos = self.state.pop_stack();
        self.state.push_stack(tos);
        self.state.push_stack(tos);
    }

    fn inst_dropd(&mut self) {
        self.state.pop_stack();
    }

    fn inst_test(&mut self) {
        let val1 = self.state.pop_stack() as i16;
        let val2 = self.state.pop_stack() as i16;
        let res = val1 - val2;
        self.state.status.zero = res == 0;
        self.state.status.negative = res < 0;
        self.state.push_stack(val2 as u16);
        self.state.push_stack(val1 as u16);
    }

    fn inst_revd(&mut self) {
        let val1 = self.state.pop_stack();
        let val2 = self.state.pop_stack();
        self.state.push_stack(val1);
        self.state.push_stack(val2);
    }

    fn inst_srup(&mut self) {
        let items_rot = self.state.pop_stack();
        let items = (items_rot & 0xFF00) >> 8;
        let rotations = (items_rot & 0x00FF) % items; // Prevent over-rotating the stack unnecessarily

        if items > self.state.isp {
            panic!(
                "Tried to rotate {} items, but only {} items exist on the instruction stack",
                items, self.state.isp
            )
        }

        // Instruction is a NOOP
        if items == rotations || items == 0 || rotations == 0 {
            return;
        }

        let mut mutable_stack = Vec::new();

        for _ in 0..items {
            let item = self.state.pop_stack();
            mutable_stack.push(item);
        }
        // Rotate the stack up
        mutable_stack.rotate_left(rotations as usize);

        for _ in 0..items {
            let item = mutable_stack
                .pop()
                .expect("Not enough items in mutable stack");
            self.state.push_stack(item);
        }
    }

    fn inst_srdn(&mut self) {
        let items_rot = self.state.pop_stack();
        let items = (items_rot & 0xFF00) >> 8;
        let rotations = (items_rot & 0x00FF) % items; // Prevent over-rotating the stack unnecessarily

        if items > self.state.isp {
            panic!(
                "Tried to rotate {} items, but only {} items exist on the instruction stack",
                items, self.state.isp
            )
        }

        // Instruction is a NOOP
        if items == rotations || items == 0 || rotations == 0 {
            return;
        }

        let mut mutable_stack = Vec::new();

        for _ in 0..items {
            let item = self.state.pop_stack();
            mutable_stack.push(item);
        }
        // Rotate the stack down
        mutable_stack.rotate_right(rotations as usize);

        for _ in 0..items {
            let item = mutable_stack
                .pop()
                .expect("Not enough items in mutable stack");
            self.state.push_stack(item);
        }
    }

    // Arithmatic operations
    instr_two_arg_unsigned_wrap!(inst_add, wrapping_add);
    instr_two_arg_signed!(inst_sub, wrapping_sub);
    instr_two_arg_signed!(inst_mul, wrapping_mul);
    instr_two_arg_signed!(inst_div, wrapping_div);
    instr_two_arg_unsigned!(inst_mod, %);

    // Logic operations
    instr_two_arg_unsigned!(inst_and, &);
    instr_two_arg_unsigned!(inst_or, |);
    instr_two_arg_unsigned!(inst_xor, ^);

    fn inst_bit_not(&mut self) {
        let val = self.state.pop_stack();
        self.state.push_stack(!val);
    }

    fn inst_ashr(&mut self) {
        let degree = self.state.pop_stack() as i16;
        let value = self.state.pop_stack() as i16;
        self.state.push_stack((value >> degree) as u16);
    }

    fn inst_ashl(&mut self) {
        let degree = self.state.pop_stack() as i16;
        let value = self.state.pop_stack() as i16;
        self.state.push_stack((value << degree) as u16);
    }

    fn inst_lshr(&mut self) {
        let degree = self.state.pop_stack();
        let value = self.state.pop_stack();
        self.state.push_stack(value >> degree);
    }

    fn inst_lshl(&mut self) {
        let degree = self.state.pop_stack();
        let value = self.state.pop_stack();
        self.state.push_stack(value << degree);
    }

    // Vector operations
    instr_vectored!(inst_addmv, +=);
    instr_vectored!(inst_mulmv, *=);
    instr_vectored!(inst_andmv, &=);
    instr_vectored!(inst_ormv, |=);

    // Program stack operations
    fn inst_loadsp(&mut self) {
        let addr = self.state.pop_stack();
        self.state.sp = addr;
    }

    fn inst_pushsp(&mut self) {
        self.state.push_stack(self.state.sp);
    }

    fn inst_push(&mut self) {
        let data = self.state.pop_stack();
        self.push_program_stack(data);
    }

    fn inst_pop(&mut self) {
        let data = self.pop_program_stack();
        self.state.push_stack(data);
    }

    // Branches
    fn inst_jmp(&mut self) {
        let addr = self.state.pop_stack();
        self.state.pc = addr;
    }

    fn inst_jmpz(&mut self) {
        if self.state.status.zero {
            let addr = self.state.pop_stack();
            self.state.pc = addr;
        }
    }

    fn inst_jmpg(&mut self) {
        if !self.state.status.zero && !self.state.status.negative {
            let addr = self.state.pop_stack();
            self.state.pc = addr;
        }
    }

    fn inst_jmpl(&mut self) {
        if !self.state.status.zero && self.state.status.negative {
            let addr = self.state.pop_stack();
            self.state.pc = addr;
        }
    }

    fn inst_jmpnz(&mut self) {
        if !self.state.status.zero {
            let addr = self.state.pop_stack();
            self.state.pc = addr;
        }
    }

    // Subroutines
    fn inst_call(&mut self) {
        self.push_program_stack(self.state.pc);
        self.state.pc = self.state.pop_stack();
    }

    fn inst_ret(&mut self) {
        self.state.pc = self.pop_program_stack();
    }

    // Software interrupt
    fn inst_swi(&mut self) {
        self.push_program_stack(self.state.pc);
        self.push_program_stack(self.state.status.as_u16());
        self.state.pc = self.read_mem_u16(INRPT_SOFTWARE);
    }

    fn inst_iret(&mut self) {
        let flags = self.pop_program_stack();
        let new_pc = self.pop_program_stack();
        self.state.status = StatusRegister::from_u16(flags);
        self.state.pc = new_pc;
    }
}
