# ZL Architecture VM

This project contains the architectural design and emulation VM for the ZL
architecture.

This project is purely academic and not intended for serious use or
implementations.

## Building

1. Install [Rust](https://www.rust-lang.org/tools/install)
2. Clone and cd into repo
3. Run `cargo build`

```shell
# Install Rust toolchain
curl https://sh.rustup.rs -sSf | sh

# Clone repo
git clone https://gitlab.com/lfkeitel/zl-arch-vm
cd zl-arch-vm

# Build (makes debug build)
cargo build
# Build release
# cargo build --release

# Binaries are available in target/
./target/debug/zlc -o ./examples/load.s19 ./examples/load.zla
./target/debug/zlsim ./examples/load.s19
```

## Executing Programs

The `run_file.sh` script will compile and execute a program in one command:

`./run_file.sh ../examples/display.zla`

## Project Layout

The repo is made up of 4 different Cargo projects.

`zlc` is the ZL Assembly compiler. It takes a text file of ZL Assembly code
and outputs an srecord formatted file of ZL machine code.

`zlsim` is the emulator. It takes an srecord formatted file of ZL machine code
executes the ZL virtual machine.

`zlsim_vm` is the core virtual machine that actually runs ZL machine code.

`zlsim_proc` is a procedural macro library that miscellaneous impl functions in
the compiler.

## License

This project is licensed under the New BSD License.
